from setuptools import setup

from usexp import name, version

setup(name=name,
      version=version,
      description='Uncanonical S-Expression Parser',
      url='https://gitlab.com/emacsen/usexp',
      author='Serge Wroclawski',
      author_email='serge@wroclawski.org',
      license='Apache License 2.0',
      packages=['usexp'],
      classifiers=[
          'Intended Audience :: Developers',
          'Programming Language :: Python',
          'Programming Language :: Python :: 3',
          'Topic :: Software Development :: Libraries',
      ]
)


      
