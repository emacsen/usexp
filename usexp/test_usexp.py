import pytest
import usexp

def test_gen_csexp_string():
    data = ["hello"]
    expected_output = b'(5:hello)'
    assert usexp.dumpb(data) == expected_output

def test_gen_csexp_sequence():
    data = [ 'a', 'b', ['c']]
    expected_output = b'(1:a1:b(1:c))'
    assert usexp.dumpb(data) == expected_output
    
def test_gen_csexp_simple():
    data = ['a', 'b', 'c', [1, 2, 3], ('d', 'e', 'f')]
    expected_output = b'(1:a1:b1:c(1:11:21:3)(1:d1:e1:f))'
    assert usexp.dumpb(data) == expected_output

def test_gen_csexp_hint():
    typed = usexp.TypeHinted('application/json', b'{}')
    data = ('a', typed, 'c')
    expected_output = b'(1:a[application/json]2:{}1:c)'
    assert usexp.dumpb(data) == expected_output

def test_load_csexp_simple():
    data = b'(1:a1:b1:c(1:11:21:3)(1:d1:e1:f))'
    expected_output = [b'a', b'b', b'c', [b'1', b'2', b'3'], [b'd', b'e', b'f']]
    assert usexp.loadb(data) == expected_output

def test_load_csexp_hint():
    data = b'([application/json]2:{})'
    expected_output = [usexp.TypeHinted('application/json', b'{}')]
    assert usexp.loadb(data) == expected_output

def test_bytes():
    data = [b'a', b'b']
    expected = b'(1:a1:b)'
    assert usexp.dumpb(data) == expected

def test_regular_and_bytes():
    # This replicates a DataShards manifest
    data = [b'manifest', 5000, b'abc', b'def', b'ghi']
    expected_output = b'(8:manifest4:50003:abc3:def3:ghi)'
    assert usexp.dumpb(data) == expected_output
    
